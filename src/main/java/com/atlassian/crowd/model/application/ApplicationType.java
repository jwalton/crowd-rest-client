/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.model.application;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Represents the type of an application.
 */
public enum ApplicationType
{
    CROWD("Crowd"),
    GENERIC_APPLICATION("Generic Application"),
    PLUGIN("Plugin"),
    JIRA("JIRA"),
    CONFLUENCE("Confluence"),
    BAMBOO("Bamboo"),
    FISHEYE("FishEye"),
    CRUCIBLE("Crucible"),
    STASH("Stash");

    private final String displayName;

    ApplicationType(String displayName)
    {
        this.displayName = displayName;
    }

    /**
     * Returns the display name of the application type.
     *
     * @return display name of the application type
     */
    public String getDisplayName()
    {
        return displayName;
    }

    /**
     * Returns the list of application types that can be added by the user to Crowd as an application.
     *
     * @return list of application types that can be added by the user to Crowd as an application.
     */
    public static List<ApplicationType> getCreatableAppTypes()
    {
        return ImmutableList.of(JIRA, CONFLUENCE, BAMBOO, FISHEYE, CRUCIBLE, STASH, GENERIC_APPLICATION);
    }
}
