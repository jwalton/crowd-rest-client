/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

/**
 * Thrown when an entity is not found.
 */
public class ObjectNotFoundException extends CrowdException
{
    public ObjectNotFoundException()
    {
        super();
    }

    public ObjectNotFoundException(Class entityClass, Object identifier)
    {
        super(new StringBuilder(64).append("Failed to find entity of type [").append(entityClass.getCanonicalName()).append("] with identifier [").append(identifier).append("]").toString());
    }

    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message the detail message
     */
    public ObjectNotFoundException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message the detail message
     * @param cause the cause
     */
    public ObjectNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Default constructor.
     * @param throwable The {@link Exception Exception}.
     */
    public ObjectNotFoundException(Throwable throwable)
    {
        super(throwable);
    }
}
